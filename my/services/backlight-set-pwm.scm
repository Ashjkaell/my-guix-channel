(define-module (my services backlight-set-pwm)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages admin)	;For the igt-gpu-tools package
  #:use-module (guix gexp)
  #:export (backlight-set-pwm-service-type
	    backlight-set-pwm-service))

(define (backlight-set-pwm-shepherd-service pwm-val)
  (shepherd-service
   (documentation "Sets the backlight PWM control value.")
   (provision '(backlight-set-pwm))
   (start #~(make-system-constructor
	     (string-append #$igt-gpu-tools "/bin/intel_reg")
	     ;; The make-system-constructor function doesn't add
	     ;; whitespace itself.
	     " write 0x00061254 " #$pwm-val))
   (one-shot? #t)))

;;; This doesn't work anymore, did the shepherd-service-type macro get
;;; updated?
;; (define backlight-set-pwm-service-type
;;   (shepherd-service-type 'backlight-set-pwm
;;                          backlight-set-pwm-shepherd-service))

(define backlight-set-pwm-service
  (service-type (name 'backlight-set-pwm)
		(extensions (list (service-extension shepherd-root-service-type
						     backlight-set-pwm-shepherd-service)))
		(description
		 "Sets the PWM value to fix uneven backlight in a
librebooted thinkpad laptop.")))

(define (backlight-set-pwm-service pwm-val)
  "Return a service adjusting the backlight PWM value (for librebooted
machines)."
  (service backlight-set-pwm-service-type pwm-val))
